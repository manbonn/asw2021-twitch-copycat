import {createContext} from 'react'

const UserContext = createContext(undefined)
const UserManipulationContext = createContext((arg) => {
})

export {
  UserContext,
  UserManipulationContext
}