import {useContext, createContext} from 'react'

const LanguageContext = createContext("en")

const LanguageManipulationContext = createContext(() => {
})

function useLanguage(strings) {
  const lang = useContext(LanguageContext)
  strings.setLanguage(lang)
}

export {
  LanguageContext,
  LanguageManipulationContext,
  useLanguage
}