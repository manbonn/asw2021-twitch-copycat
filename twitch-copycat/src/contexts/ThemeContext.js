import {createContext, useContext} from 'react'

const ThemeContext = createContext("original")

const ThemeManipulationContext = createContext(() => {
})

function useTheme() {
  const theme = useContext(ThemeContext)

  switch (theme) {
    case 'original':
      return commonThemes.original
    case 'dark':
      return commonThemes.dark
    case 'protanopia':
      return commonThemes.protanopia
    case 'deuteranopia':
      return commonThemes.deuteranopia
    case 'tritanopia':
      return commonThemes.tritanopia
    default:
      return commonThemes.original
  }
}

const commonThemes = {
  original: {
    mainColor: "#ffe3ce",
    secondaryColor: "#2fc9d4",
    primaryText: "#000000",
    dilutedMainColor: "#ff9c59",
    dilutedSecondaryColor: "#a0eafa",
    dilutedSecondaryText: "#555555",
    linkColorHeader: "#555555",
    linkColorMain: "#65bfff",
    errorColor: "#bf0000"
  },
  dark: {
    mainColor: "#404040",
    secondaryColor: "#202225",
    primaryText: "#999999",
    dilutedMainColor: "#ff9c59",
    dilutedSecondaryColor: "#767b81",
    dilutedSecondaryText: "#555555",
    linkColorHeader: "#555555",
    linkColorMain: "#65bccf",
    errorColor: "#bf0000"
  },
  protanopia: {
    mainColor: "#ffe3ce",
    secondaryColor: "#65bccf",
    primaryText: "#000000",
    dilutedMainColor: "#ff9c59",
    dilutedSecondaryColor: "#bff4ff",
    dilutedSecondaryText: "#555555",
    linkColorHeader: "#555555",
    linkColorMain: "#65bccf",
    errorColor: "#bf0000"
  },
  deuteranopia: {
    mainColor: "#ffe3ce",
    secondaryColor: "#65bccf",
    primaryText: "#000000",
    dilutedMainColor: "#ff9c59",
    dilutedSecondaryColor: "#bff4ff",
    dilutedSecondaryText: "#555555",
    linkColorHeader: "#555555",
    linkColorMain: "#65bccf",
    errorColor: "#bf0000"
  },
  tritanopia: {
    mainColor: "#ffe3ce",
    secondaryColor: "#65bccf",
    primaryText: "#000000",
    dilutedMainColor: "#ff9c59",
    dilutedSecondaryColor: "#bff4ff",
    dilutedSecondaryText: "#555555",
    linkColorHeader: "#555555",
    linkColorMain: "#65bccf",
    errorColor: "#bf0000"
  }
}

export {
  ThemeContext,
  ThemeManipulationContext,
  useTheme
};