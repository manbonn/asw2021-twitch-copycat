import {createContext} from 'react'

const LiveRoutesManipulationContext = createContext(() => {
})

const ReplayRoutesManipulationContext = createContext(() => {
})

export {
  LiveRoutesManipulationContext,
  ReplayRoutesManipulationContext,
}