import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import art from "assets/images/art.png";
import bannerOriginal from "assets/images/banner-original.png";
import bannerDark from "assets/images/banner-dark.png";
import bannerPortraitOriginal from "assets/images/asw-original.png";
import bannerPortraitDark from "assets/images/asw-dark.png";

import UserProfileButton from "components/header/UserProfileButton";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {UserContext} from "contexts/UserContext";
import Categories from "scenes/Categories";
import Forbidden from "scenes/Forbidden";
import Logout from "scenes/Logout";
import UserProfile from "scenes/UserProfile";
import UserStream from "scenes/UserStream";
import {appendStyle} from "styles/common";
import {ThemeContext, useTheme} from "contexts/ThemeContext";
import React, {useContext, useEffect, useRef, useState} from 'react';
import {BrowserRouter, Link, Route, Switch, useLocation} from "react-router-dom";
import {Button} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import 'styles/header-rwd.css';
import 'styles/categories-rwd.css';
import LocalizedStrings from 'react-localization';

import SpinningCharmander from "components/SpinningCharmander";
import SearchBar from "components/header/SearchBar";
import ProfileButton from "components/header/ProfileButton";
import Copyright from "components/footer/Copyright";
import Disclaimer from "components/footer/Disclaimer";
import SocialBar from "components/footer/SocialBar";

import Broadcast from "scenes/Broadcast";
import Faq from "scenes/Faq";
import Feedback from "scenes/Feedback";
import Credits from "scenes/Credits";
import Login from "scenes/Login";
import Missing from "scenes/Missing";
import Privacy from "scenes/Privacy";
import Signup from "scenes/Signup";
import SiteMap from "scenes/SiteMap";
import StreamList from "scenes/StreamList";
import Tos from "scenes/Tos";

function ScrollToTop() {
  const {pathname} = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}

const Header = () => {
  const theme = useTheme()
  const user = useContext(UserContext)
  const themeName = useContext(ThemeContext)

  return (
    <>
      <header className={"header-landscape"}
              style={appendStyle(styles.header, "backgroundColor", theme.secondaryColor)}>
        {clickableBanner(themeName)}
        {browseButton()}
        {categoriesButton()}
        {broadcastButton()}
        <SearchBar/>
        {loginButton(user)}
        {signupButton(user)}
        {profileButtonHeader(user)}
        {profilePicButton(user)}
      </header>
      <header className={"header-portrait"} style={appendStyle(styles.header, "backgroundColor", theme.secondaryColor)}>
        <Hamburger/>
        {switchBannerPortrait(themeName)}
        {user === undefined ? <ProfileButton/> : <UserProfileButton/>}
      </header>
    </>
  )
}

function MainPage({liveRoutes}) {
  useLanguage(strings)
  const theme = useContext(ThemeContext)

  return (
    <BrowserRouter>
      <ScrollToTop/>
      <Header/>
      <main style={appendStyle(styles.main, "backgroundColor", theme.mainColor)}>
        <Switch>
          <Route path={"/login"}><Login/></Route>
          <Route path={"/profile"}><UserProfile/></Route>
          <Route path={"/signup"}><Signup/></Route>
          <Route path={"/streams/art"}><StreamList from={"art"}/></Route>
          <Route exact path={"/streams"}><StreamList from={"all"}/></Route>
          <Route path={"/broadcast"}><Broadcast/></Route>
          <Route path={"/categories"}> <Categories/> </Route>
          <Route path={"/credits"}><Credits/></Route>
          <Route path={"/faq"}><Faq/></Route>
          <Route path={"/feedback"}><Feedback/></Route>
          <Route path={"/streams/gaming"}><StreamList from={"gaming"}/></Route>
          <Route exact path={"/"}><SpinningCharmander/></Route>
          <Route path={"/streams/irl"}><StreamList from={"irl"}/></Route>
          <Route path={"/streams/music"}><StreamList from={"music"}/></Route>
          <Route path={"/privacy"}><Privacy/></Route>
          <Route path={"/sitemap"}><SiteMap/></Route>
          <Route path={"/tos"}><Tos/></Route>
          {liveRoutes}
          <Route path={"/user/:user"}><UserStream/></Route>
          <Route path={"/logout"}><Logout/></Route>
          <Route path={"/forbidden"} component={Forbidden}/>
          <Route component={Missing}/>
        </Switch>
      </main>
      <Footer/>
    </BrowserRouter>
  )
}

const Footer = () => {
  const theme = useTheme()
  return (
    <footer style={appendStyle(styles.footer, "backgroundColor", theme.secondaryColor)}>
      <Disclaimer/>
      <nav className={"links"} style={styles.links}>
        {tosButton(theme)}
        {privacyButton(theme)}
        {faqButton(theme)}
        {feedbackButton(theme)}
        {creditsButton(theme)}
        {sitemapButton(theme)}
      </nav>
      <SocialBar/>
      <Copyright/>
    </footer>
  )
}

const loginButton = (user) => {
  if (user === undefined) {
    return <Link to={"/login"}><Button><P>{strings.login}</P></Button></Link>
  } else {
    return <></>
  }
}

const signupButton = (user) => {
  if (user === undefined) {
    return <Link to={"/signup"}><Button><P>{strings.signup}</P></Button></Link>
  } else {
    return <></>
  }
}

const profileButtonHeader = (user) => {
  if (user === undefined) {
    return <></>
  } else {
    return <Link to={"/profile"}><Button><P>{strings.profile}</P></Button></Link>
  }
}

const profilePicButton = (user) => {
  if (user === undefined) {
    return <ProfileButton/>
  } else {
    return <UserProfileButton/>
  }
}

function Hamburger() {
  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);
  useLanguage(strings)

  function handleClose(event) {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  }

  function toggle() {
    setOpen((prevOpen) => !prevOpen);
  }

  return (
    <div className={"hamburger"}>
      <Button ref={anchorRef} onClick={toggle}>
        <MenuIcon/>
      </Button>
      <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition>
        {({TransitionProps, placement}) => (
          <Grow {...TransitionProps} style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}>
            <Paper style={styles.growPopper}>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList autoFocusItem={open} id="menu-list-grow">
                  <Link to={"/streams"}><MenuItem onClick={handleClose}>{strings.browse}</MenuItem></Link>
                  <Link to={"/categories"}><MenuItem onClick={handleClose}>{strings.categories}</MenuItem></Link>
                  <Link to={"/broadcast"}><MenuItem onClick={handleClose}>{strings.broadcast}</MenuItem></Link>
                  <MenuItem onClick={handleClose}>{strings.search}</MenuItem>
                  <Link to={"/login"}><MenuItem onClick={handleClose}>{strings.login}</MenuItem></Link>
                  <Link to={"/signup"}><MenuItem onClick={handleClose}>{strings.signup}</MenuItem></Link>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
}

const clickableBanner = (themeName) => {
  return (
    <Link to={"/"} style={styles.banner}>
      {switchBannerLandscape(themeName)}
    </Link>
  );
}

function switchBannerPortrait(themeName) {
  switch (themeName) {
    case "original":
      return <img src={bannerPortraitOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    case "dark":
      return <img src={bannerPortraitDark} style={styles.banner} alt={"A Streaming Website"}/>
    case "tritanopia":
      return <img src={bannerPortraitOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    case "protanopia":
      return <img src={bannerPortraitOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    case "deuteranopia":
      return <img src={bannerPortraitOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    default:
      return <img src={bannerPortraitOriginal} style={styles.banner} alt={"A Streaming Website"}/>
  }
}

function switchBannerLandscape(themeName) {
  switch (themeName) {
    case "original":
      return <img src={bannerOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    case "dark":
      return <img src={bannerDark} style={styles.banner} alt={"A Streaming Website"}/>
    case "tritanopia":
      return <img src={bannerOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    case "protanopia":
      return <img src={bannerOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    case "deuteranopia":
      return <img src={bannerOriginal} style={styles.banner} alt={"A Streaming Website"}/>
    default:
      return <img src={bannerOriginal} style={styles.banner} alt={"A Streaming Website"}/>
  }
}

const browseButton = () => {
  return <Link to={"/streams"}><Button><P>{strings.browse}</P></Button></Link>
}

const categoriesButton = () => {
  return <Link to={"/categories"}><Button><P>{strings.categories}</P></Button></Link>
}

const broadcastButton = () => {
  return <Link to={"/broadcast"}><Button><P>{strings.broadcast}</P></Button></Link>
}

const tosButton = (theme) => {
  return <Link style={appendStyle(styles.spaced, "color", theme.linkColorHeader)} to={"/tos"}>{strings.tos}</Link>
}

const privacyButton = (theme) => {
  return <Link to={"/privacy"}
               style={appendStyle(styles.spaced, "color", theme.linkColorHeader)}>{strings.privacy}</Link>
}

const faqButton = (theme) => {
  return <Link to={"/faq"} style={appendStyle(styles.spaced, "color", theme.linkColorHeader)}>{strings.faq}</Link>
}

const feedbackButton = (theme) => {
  return <Link to={"/feedback"}
               style={appendStyle(styles.spaced, "color", theme.linkColorHeader)}>{strings.feedback}</Link>
}

const creditsButton = (theme) => {
  return <Link to={"/credits"}
               style={appendStyle(styles.spaced, "color", theme.linkColorHeader)}>{strings.credits}</Link>
}

const sitemapButton = (theme) => {
  return <Link to={"/sitemap"}
               style={appendStyle(styles.spaced, "color", theme.linkColorHeader)}>{strings.sitemap}</Link>
}

const strings = new LocalizedStrings({
  en: {
    browse: "Browse",
    broadcast: "Broadcast",
    categories: "Categories",
    search: "Search",
    credits: "Credits",
    sitemap: "Sitemap",
    login: "Login",
    signup: "Signup",
    tos: "T.O.S.",
    privacy: "Privacy",
    faq: "F.A.Q.",
    feedback: "Feedback",
    titleCategories: "Categories",
    profile: "Profile",
  },
  it: {
    browse: "Esplora",
    broadcast: "Trasmetti",
    categories: "Categorie",
    search: "Cerca",
    credits: "Crediti",
    sitemap: "Mappa del sito",
    login: "Login",
    signup: "Signup",
    tos: "T.O.S.",
    privacy: "Privacy",
    faq: "F.A.Q.",
    feedback: "Feedback",
    titleCategories: "Categorie",
    profile: "Profilo",
  },
  ru: {
    browse: "Изучите",
    broadcast: "Транслировать",
    credits: "Кредиты",
    search: "Искайте",
    sitemap: "карта сайта ",
    login: "Авторизоваться",
    signup: "зарегистрироваться",
    tos: "Условия использования ",
    privacy: "политика конфиденциальности",
    faq: "ЧЗВ",
    feedback: "Обратная связь ",
    titleCategories: "Категории",
    profile: "",
  }
});

const styles = {
  header: {
    width: "calc(100vw - (100vw - 100%))",
    height: "10vh",
    minHeight: "10vh",
    maxHeight: "10vh",
  },
  banner: {
    height: "10vh"
  },
  main: {
    minHeight: "90vh",

    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: "3vh",
  },
  footer: {

    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  links: {
    marginTop: "1vh",
    marginBottom: "2vh"
  },
  social: {
    display: "flex",
    flexDirection: "row",
    alignItems: "",
    justifyContent: ""
  },
  socialIcon: {
    marginLeft: "1vw",
    marginRight: "1vw",
    width: "32px",
    height: "32px"
  },
  spaced: {
    marginRight: "3px",
    marginLeft: "3px",
  },
  image: {
    position: "relative"
  },
  categoryImage: {
    filter:
      " drop-shadow(12px 0 0 #ee7326)" +
      " drop-shadow(0 6px 0 #ee7326)" +
      " drop-shadow(0 -6px 0 #ee7326)" +
      " drop-shadow(-12px 0 0 #ee7326)"
  },
  overlay: {
    transform: "rotate(-45deg)",
    fontSize: "3vw",
    fontWeight: "700",
    color: "#dddddd",
    textShadow: " 3px 3px 0 #ee7326, -1px -1px 0 #ee7326, 1px -1px 0 #ee7326, -1px 1px 0 #ee7326, 1px 1px 0 #ee7326"
  },
  growPopper: {
    backgroundColor: "#bff4ff"
  }
}

export default MainPage;