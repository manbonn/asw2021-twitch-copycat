import {sha256} from 'js-sha256';

const PHONEBOOK = {
  WEBSITE: "http://localhost:3000",
  EXPRESS: "http://localhost:3500",
}

function checkValidWord(value) {
  if (value) {
    const word = /^[a-zA-Z]+$/
    return word.test(value)
  }
  return undefined
}

function checkValidEmail(value) {
  const mail = /\w+[@]\w+[.]\w+/
  return mail.test(value)
}

function checkValidPhone(value) {
  if (value) {
    const phone = /^\d{10,}$/
    return phone.test(value)
  }
  return false
}

function checkValidPassword(value) {
  if (value) {
    return value.length >= 6;
  }
  return false
}

function checkValidConfirmationPassword(value, other) {
  return value === other
}

function fetchStreams() {
  return fetch(PHONEBOOK.EXPRESS + "/api/streams", {
      method: "get",
      headers: {"Content-Type": "application/json"},
    }
  )
    .then(res => res.text())
    .catch()
}

function fetchUsers() {
  return fetch(PHONEBOOK.EXPRESS + "/api/users", {
    method: "get",
    headers: {"Content-Type": "application/json"},
  })
    .then(res => res.text())
    .catch()
}

function searchQuery(value) {
  return fetch(PHONEBOOK.EXPRESS + "/api/search/" + value, {
    method: "get",
    headers: {"Content-Type": "application/json"}
  })
    .then(res => res.text())
    .catch()
}

function onEnter(event, handler) {
  if (event.key === "enter") {
    handler();
  }
}

function ArrayBufferStringify(buffer) {
  return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

function checkValidUser(value) {
  return checkValidEmail(value) || checkValidWord(value)
}

function digestSha256(message) {
  return sha256(message)
}

export {
  PHONEBOOK,
  checkValidEmail,
  checkValidPhone,
  checkValidWord,
  checkValidConfirmationPassword,
  checkValidPassword,
  ArrayBufferStringify,
  fetchStreams,
  fetchUsers,
  onEnter,
  searchQuery,
  checkValidUser,
  digestSha256,
}