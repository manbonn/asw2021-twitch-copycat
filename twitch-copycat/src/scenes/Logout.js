import {UserManipulationContext} from "contexts/UserContext";
import React, {useContext, useEffect} from 'react';
import {useHistory} from "react-router-dom";

function Logout() {
  const setUser = useContext(UserManipulationContext)
  const history = useHistory()

  useEffect(() => {
    setUser(undefined)
    setTimeout(() => {
      history.push("/")
    }, 500)
  }, [])

  return (
    <div>{"Logout"}</div>
  );
}

export default Logout;