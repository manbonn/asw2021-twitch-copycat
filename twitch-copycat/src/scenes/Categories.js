import art from "assets/images/art.png";
import gaming from "assets/images/gaming.png";
import irl from "assets/images/irl.png";
import music from "assets/images/music.png";
import 'styles/categories-rwd.css';
import React from 'react';
import {Link} from "react-router-dom";
import LocalizedStrings from "react-localization";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";

function Categories() {
  useLanguage(strings)
  const theme = useTheme()

  return (
    <section className={"categories"}>
      <Link id={"irl"} className={"image"} style={styles.image} to={"/streams/irl"}>
        <img style={styles.categoryImage} src={irl} alt={strings.irlAlternative}/>
        <P style={styles.overlay}>I.R.L.</P>
      </Link>
      <Link id={"gaming"} className={"image"} style={styles.image} to={"/streams/gaming"}>
        <img style={styles.categoryImage} src={gaming} alt={strings.gamingAlternative}/>
        <P style={styles.overlay}>Games</P>
      </Link>
      <Link id={"music"} className={"image"} style={styles.image} to={"/streams/music"}>
        <img style={styles.categoryImage} src={music} alt={strings.musicAlternative}/>
        <P style={styles.overlay}>Music</P>
      </Link>
      <Link id={"art"} className={"image"} style={styles.image} to={"/streams/art"}>
        <img style={styles.categoryImage} src={art} alt={strings.artAlternative}/>
        <P style={styles.overlay}>Art</P>
      </Link>
    </section>
  );
}

const styles = {
  categoryImage: {
    filter:
      " drop-shadow(12px 0 0 #ee7326)" +
      " drop-shadow(0 6px 0 #ee7326)" +
      " drop-shadow(0 -6px 0 #ee7326)" +
      " drop-shadow(-12px 0 0 #ee7326)"
  },
  overlay: {
    transform: "rotate(-45deg)",
    fontSize: "3vw",
    fontWeight: "700",
    color: "#dddddd",
    textShadow: " 3px 3px 0 #ee7326, -1px -1px 0 #ee7326, 1px -1px 0 #ee7326, -1px 1px 0 #ee7326, 1px 1px 0 #ee7326"
  },
  image: {
    position: "relative"
  },
}

const strings = new LocalizedStrings({
  en: {
    irlAlternative: "In real life video streaming",
    gamingAlternative: "Gaming video streaming",
    musicAlternative: "Music video streaming",
    artAlternative: "Art video streaming"
  },
  it: {
    irlAlternative: "In real life video streaming",
    gamingAlternative: "Video streaming gaming",
    musicAlternative: "Video streaming musicali",
    artAlternative: "Video streaming artistici"
  },
  ru: {
    irlAlternative: "Болтать",
    gamingAlternative: "Игры",
    musicAlternative: "музыкальный",
    artAlternative: "артистический"
  }
});


export default Categories;