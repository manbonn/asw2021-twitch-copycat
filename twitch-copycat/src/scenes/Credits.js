import A from "components/A";
import H from "components/H";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import React from 'react';
import LocalizedStrings from "react-localization";
import {common} from "styles/common";
import Divider from '@material-ui/core/Divider';

function Credits() {
  useLanguage(strings)

  const nintendo = "https://assets.pokemon.com/assets/cms2/img/cards/web/SWSH4/SWSH4_EN_23.png"
  const spinning = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/f54147ad-0799-4c2c-a24b-031984ca3625/d92udii-53d7516a-9764-468d-b5a7-9da05e7ff59a.png/v1/fill/w_400,h_400,strp/charmander_firefox_logo_by_sribbleinc_d92udii-fullview.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD00MDAiLCJwYXRoIjoiXC9mXC9mNTQxNDdhZC0wNzk5LTRjMmMtYTI0Yi0wMzE5ODRjYTM2MjVcL2Q5MnVkaWktNTNkNzUxNmEtOTc2NC00NjhkLWI1YTctOWRhMDVlN2ZmNTlhLnBuZyIsIndpZHRoIjoiPD00MDAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.gQhAVOUP3N4N_t993U6yf7jDsKXqKTo1P_-Kf0Ojees"
  const cocktail = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e3c7c11e-dea2-4d25-864b-eade9835dc75/dafcpqz-7b48432f-8e75-4cc2-9a5e-59c1f568c58b.jpg/v1/fill/w_894,h_894,q_70,strp/charmander_sketch_by_naschi_dafcpqz-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD0xMDI0IiwicGF0aCI6IlwvZlwvZTNjN2MxMWUtZGVhMi00ZDI1LTg2NGItZWFkZTk4MzVkYzc1XC9kYWZjcHF6LTdiNDg0MzJmLThlNzUtNGNjMi05YTVlLTU5YzFmNTY4YzU4Yi5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.TzfcElCU3WsHk7As19v2EIRINWpxwx83j_KkBtwhKwI"
  const ukulele = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/5c16868d-672d-495b-af33-857cac5dc4c5/d8nesse-a7d24546-523e-47d6-a167-583ff66e23c5.jpg/v1/fill/w_894,h_894,q_70,strp/ukulele_charmander_by_bluukiss_d8nesse-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD0xMjgwIiwicGF0aCI6IlwvZlwvNWMxNjg2OGQtNjcyZC00OTViLWFmMzMtODU3Y2FjNWRjNGM1XC9kOG5lc3NlLWE3ZDI0NTQ2LTUyM2UtNDdkNi1hMTY3LTU4M2ZmNjZlMjNjNS5qcGciLCJ3aWR0aCI6Ijw9MTI4MCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.KPPi8mu4PsjdzvGDpYhsGSQOpcdNV5zduVco2A_iGK4"
  const pudding = "https://pbs.twimg.com/media/Etlw7hiU0AEHrKw?format=jpg&name=small"
  const gameboy = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/5a2fa819-c613-46f9-a160-bf1d7712b354/dagaxu1-2d418ba3-e7bb-4bd3-9c8d-b2cb2c97a08f.jpg/v1/fill/w_1024,h_1511,q_75,strp/charmander_playing___good_old_times_by_ptiluky_dagaxu1-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD0xNTExIiwicGF0aCI6IlwvZlwvNWEyZmE4MTktYzYxMy00NmY5LWExNjAtYmYxZDc3MTJiMzU0XC9kYWdheHUxLTJkNDE4YmEzLWU3YmItNGJkMy05YzhkLWIyY2IyYzk3YTA4Zi5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.uDkcrQ-AEV-4DjptFetifGv5RseM9s0v2BYiNbx0McA"
  return (
    <section>
      <H number={1}>{strings.credits}</H>
      <P style={styles.centered}>{strings.longCredit}</P>
      <Credit src={nintendo} author={"Atsuko Nishida"} title={"004 - Charmander"}
              links={["https://nintendo.com", "https://bulbapedia.bulbagarden.net/wiki/Atsuko_Nishida"]}/>
      <Divider orientation={"horizontal"}/>
      <Credit src={spinning} author={"sribbleinc"} title={"Charmander-Firefox logo"}
              links={["https://www.deviantart.com/sribbleinc", "https://www.deviantart.com/sribbleinc/gallery"]}/>
      <Divider orientation={"horizontal"}/>
      <Credit src={cocktail} author={"naschi"} title={"Charmander sketch"}
              links={["https://www.deviantart.com/naschi", "https://www.deviantart.com/naschi/gallery", "https://beacons.ai/nashimanga"]}/>
      <Divider orientation={"horizontal"}/>
      <Credit src={ukulele} author={"bluukiss"} title={"Ukulele Charmander"}
              links={["https://www.deviantart.com/bluukiss", "https://www.deviantart.com/bluukiss/gallery", "https://linktr.ee/vivianlim.art"]}/>
      <Divider orientation={"horizontal"}/>
      <Credit src={pudding} author={"uno_yu_ji"} title={"# Kore de forowā-san fuemashita"}
              links={["https://twitter.com/uno_yu_ji"]}/>
      <Divider orientation={"horizontal"}/>
      <Credit src={gameboy} author={"ptiluky"} title={"Charmander playing - Good old times"}
              links={["https://www.deviantart.com/ptiluky", "https://www.facebook.com/LucasHerbin", "https://twitter.com/LucasHerbin"]}/>
      <Divider orientation={"horizontal"}/>
    </section>
  );
}

function Credit({src, author, description, title, links}) {
  return (
    <div style={styles.credits}>
      <H number={3}>{+author + " for: " + title}</H>
      <img style={styles.viewable} src={src} alt={src}/>
      <P>{description}</P>
      <H number={5} style={common.zeroed}>{strings.contacts}</H>
      {links.map((it) => {
        return (<><A href={it} target="_blank" rel="noreferrer noopener">{it}</A><br/></>)
      })}
    </div>
  );
}

const strings = new LocalizedStrings({
  en: {
    credits: "Credits",
    thanks: "Thank you, ",
    contacts: "Contacts",
    longCredit: "All the rightful intellectual property of the drawings around the website belongs to their rightful owners, expressed as follows. Thank you all very much.",
  },
  it: {
    credits: "Crediti",
    thanks: "Grazie, ",
    contacts: "Contatti",
    longCredit: "La proprietà intellettuale di qualsiasi disegno in questo sito appartiene ai rispettivi creatori, elencati di seguito.",
  },
  ru: {
    credits: "чести",
    thanks: "Спасибо, ",
    contacts: "контакты",
    longCredit: "",
  }
});

const styles = {
  credits: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: "15px"
  },
  viewable: {
    maxWidth: "calc(100vw - (100vw - 100%))",
    height: "auto"
  },
  centered: {
    textAlign: "center"
  }
}

export default Credits;