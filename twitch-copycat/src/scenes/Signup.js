import {Paper, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import H from "components/H";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import {UserContext, UserManipulationContext} from "contexts/UserContext";
import React, {useContext, useEffect, useState} from 'react';
import LocalizedStrings from "react-localization";
import {useHistory} from "react-router-dom";
import Forbidden from "scenes/Forbidden";
import {appendStyle, common} from "styles/common";

import {checkValidEmail, checkValidPassword, checkValidWord, digestSha256, PHONEBOOK} from "utils/common";

function Signup() {
  const theme = useTheme()
  const loggedUser = useContext(UserContext)
  const history = useHistory()
  useLanguage(strings)

  const userSetter = useContext(UserManipulationContext)
  const [username, setUsername] = useState("")
  const [validUsername, setValidUsername] = useState(undefined)
  const [errorUsername, setErrorUsername] = useState(false)
  const [usernameTaken, setUsernameTaken] = useState(false)
  const [email, setEmail] = useState("")
  const [validMail, setValidMail] = useState(undefined)
  const [errorEmail, setErrorEmail] = useState(false)
  const [mailTaken, setMailErrorTaken] = useState(undefined)
  const [password, setPassword] = useState("")
  const [validPassword, setValidPassword] = useState(undefined)
  const [errorPassword, setErrorPassword] = useState(false)
  const [confirmPassword, setConfirmPassword] = useState("")
  const [validConfirmPassword, setValidConfirmPassword] = useState(undefined)
  const [errorConfirmPassword, setErrorConfirmPassword] = useState(false)
  const [infoError, setInfoError] = useState({usernameTaken: undefined, mailTaken: undefined})
  const [profileSwitch, setProfileSwitch] = useState(false)

  useEffect(() => {
    if (profileSwitch) {
      history.push("/profile")
    }
  }, [profileSwitch])

  useEffect(() => {
    const uTaken = infoError.usernameTaken
    const mTaken = infoError.mailTaken
    if (uTaken !== undefined && mTaken !== undefined) {
      if (!uTaken && !mTaken) {
        const digest = digestSha256(password)
        fetch(PHONEBOOK.EXPRESS + "/api/login", {
          method: "post",
          headers: {"Content-Type": "application/json"},
          body: JSON.stringify({
            method: "username",
            user: username,
            password: digest
          })
        })
          .then(res => res.text())
          .then(JSON.parse)
          .then(it => userSetter(it))
          .then(() => setProfileSwitch(true))
          .catch(it => console.log(it + " error"))
      }
      if (uTaken) setUsernameTaken(true)
      if (mTaken) setMailErrorTaken(true)
    }
  }, [infoError])

  useEffect(() => {
    setValidUsername(checkValidWord(username))
  }, [username])

  useEffect(() => {
    setValidMail(checkValidEmail(email))
  }, [email])

  useEffect(() => {
    setValidPassword(checkValidPassword(password))
  }, [password])

  useEffect(() => {
    setValidConfirmPassword(password === confirmPassword)
  }, [password, confirmPassword])

  function normalizeData() {
    setUsername(current => current.trim())
    setEmail(current => current.trim())
  }

  function checkAll() {
    if (!validUsername) setErrorUsername(true)
    if (!validMail) setErrorEmail(true)
    if (!validPassword) setErrorPassword(true)
    if (!confirmPassword) setErrorConfirmPassword(true)
    return validUsername &&
      validMail &&
      validPassword &&
      validConfirmPassword
  }

  function sendInfo() {
    if (checkAll()) {
      normalizeData()
      const digest = digestSha256(password)
      fetch(PHONEBOOK.EXPRESS + "/api/registration", {
        method: "post",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
          username: username,
          email: email,
          password: digest
        })
      })
        .then(res => res.text())
        .then(JSON.parse)
        .then(info => setInfoError(info))
        .catch(it => console.log(it + "error in signup"))
    }
  }

  if (loggedUser !== undefined) {
    return (<Forbidden user={true}/>)
  } else {
    return (
      <section>
        <H number={3}>{strings.signup}</H>
        <H number={6}>{strings.fieldsRequirements}</H>
        <H number={6}>{strings.passwordRequirements}</H>
        <Paper style={styles.paper}>
          <TextField label={strings.usernameLabel}
                     style={styles.row}
                     required
                     error={!validUsername && errorUsername}
                     onChange={(event) => {
                       setUsername(event.target.value)
                     }}
                     value={username}
                     helperText={usernameTaken ? strings.usernameTaken : errorUsername ? strings.usernameRequirements : ""}
                     variant="outlined"/>
          <TextField label={strings.emailLabel}
                     style={styles.row}
                     required
                     error={!validMail && errorEmail}
                     onChange={(event) => {
                       setEmail(event.target.value)
                     }}
                     helperText={mailTaken ? strings.mailtaken : errorEmail ? strings.mailRequirements : ""}
                     value={email}
                     variant="outlined"/>
          <div style={{...common.row, ...styles.row}}>
            <TextField label={strings.passwordLabel}
                       style={styles.distant}
                       required
                       error={!validPassword && errorPassword}
                       onChange={(event) => {
                         setPassword(event.target.value)
                       }}
                       value={password}
                       helperText={errorPassword ? strings.passwordRequirements : ""}
                       type="password"
                       autoComplete="current-password"
                       variant="outlined"/>
            <TextField label={strings.confirmPasswordLabel}
                       required
                       error={!validConfirmPassword && errorConfirmPassword}
                       onChange={(event) => {
                         setConfirmPassword(event.target.value)
                       }}
                       value={confirmPassword}
                       type="password"
                       autoComplete="current-password"
                       variant="outlined"/>
          </div>
          <Button style={appendStyle(styles.button, "backgroundColor", theme.dilutedSecondaryColor)}
                  onClick={() => sendInfo()}>
            {strings.signupLabel}
          </Button>
        </Paper>
      </section>
    );

  }
}

const strings = new LocalizedStrings({
  en: {
    signup: "Fill up all the fields to register a new account",
    usernameLabel: "Username",
    emailLabel: "E-mail",
    passwordLabel: "Password",
    confirmPasswordLabel: "Confirm password",
    tosAgreement: "I read and agreed to the",
    tos: "Terms of Service.",
    signupLabel: "Signup",
    fieldsRequirements: "All fields are mandatory",
    passwordRequirements: "Password must be at least 6 characters long",
    passwordMismatch: "The two passwords must match.",
    mailRequirements: "The mail is expected in the username@domain",
    usernameRequirements: "This field is mandatory",
    mailtaken: "The email is taken",
    usernameTaken: "The username is taken",

  },
  it: {
    signup: "Completa tutti i campi per registrare un account",
    usernameLabel: "Username",
    emailLabel: "E-mail",
    passwordLabel: "Password",
    confirmPasswordLabel: "Conferma password",
    tosAgreement: "Ho letto e accettato i",
    tos: "Termini di Servizio.",
    signupLabel: "Registrati",
    fieldsRequirements: "Tutti i campi sono obbligatori",
    passwordRequirements: "La password deve contenere almeno 6 caratteri",
    passwordMismatch: "Le due password devono coincidere",
    mailRequirements: "La mail deve essere nel formato username@dominio",
    usernameRequirements: "Questo campo è obbligatorio",
    mailtaken: "La mail è già in utilizzo",
    usernameTaken: "Il nome utente è già in utilizzo",

  },
  ru: {
    signup: "TBT",
    usernameLabel: "Пользователя",
    emailLabel: "Имейл",
    passwordLabel: "Пароль",
    confirmPasswordLabel: "Повторите пароль",
    tosAgreement: "TBT",
    tos: "TBT",
    signupLabel: "записаться",
    fieldsRequirements: "",
    passwordRequirements: "",
    passwordMismatch: "",
    mailRequirements: "",
    usernameRequirements: "",
    mailtaken: "",
    usernameTaken: "",

  }
});

const styles = {
  section: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignContent: "center"
  },
  forgotten: {
    textAlign: "right",
    fontSize: "0.75em"
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    paddingTop: "1%",
    paddingBottom: "2%",
    paddingLeft: "3%",
    paddingRight: "3%",
    backgroundColor: "transparent"
  },
  row: {
    marginTop: "2%",
    fontSize: "0.9em",
    wordWrap: "none"
  },
  button: {
    marginTop: "5%"
  },
  distant: {
    marginRight: "1.5%"
  }
}

export default Signup;