import {Card} from "@material-ui/core";
import A from "components/A";
import H from "components/H";
import P from "components/P";
import Stream from "components/stream/Stream";
import {LiveRoutesManipulationContext} from "contexts/InnerRoutingContext";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React, {useContext, useEffect, useState} from 'react';
import LocalizedStrings from "react-localization";
import {Route, useHistory} from "react-router-dom";
import {appendStyle, common} from "styles/common";
import {fetchStreams, PHONEBOOK} from "utils/common";

function StreamList(props) {
  useLanguage(strings)
  const theme = useTheme()
  const liveRoutesHandler = useContext(LiveRoutesManipulationContext)

  const [data, setData] = useState([])
  let routes = [];

  useEffect(() => {
    fetchStreams().then(JSON.parse).then(setData);
  }, [])

  useEffect(() => {
    if (data !== undefined) {

      routes =
        data.map((res, index) => {
          return (
            <Route key={index} path={"/user/" + res.username}>
              <Stream
                key={index}
                username={res.username}
                liveSource={res.liveSource}
                title={res.title}
                preview={res.preview}
                topic={res.topic}
                category={res.category}
                viewers={res.viewers}
                tags={res.tags}
              />
            </Route>
          )
        })
    }
  }, [data])

  useEffect(() => {
    liveRoutesHandler(routes)
  }, [data])

  useEffect(() => {
    let timeout;
    const retriever = () => {
      fetchStreams().then(JSON.parse).then(setData);
      timeout = setTimeout(retriever, 5000);
    };
    timeout = setTimeout(retriever, 5000);
    return () => clearTimeout(timeout);
  }, [data]);

  return (
    <div style={common.column}>
      <H number={2}>{pickCategory(props.from)}</H>
      <div style={appendStyle({...common.row, ...styles.wrapping}, "justifyContent", "center")}>
        {data.map((it, index) => {
          return (
            <StreamPreview key={index} properties={it}/>
          )
        })}
      </div>
    </div>
  );
}

function StreamPreview({properties}) {
  const theme = useTheme()
  const history = useHistory()

  let categoryStyle = {...styles.categories, ...common.plainLink}
  categoryStyle = appendStyle(categoryStyle, "backgroundColor", theme.dilutedSecondaryColor)
  categoryStyle = appendStyle(categoryStyle, "color", theme.dilutedSecondaryText)
  return (
    <Card style={styles.separated}>
      <img onClick={() => history.push("/user/" + properties.streamer)}
           style={{...styles.preview, ...styles.clickable}}
           src={properties.preview}
           alt={properties.streamer}/>
      <div style={styles.clickable} onClick={() => history.push("/user/" + properties.streamer)}>
        <P>{properties.title}</P>
      </div>
      <div style={styles.clickable} onClick={() => history.push("/user/" + properties.streamer)}>
        <P>{properties.streamer}</P>
      </div>
      <div style={{...common.row, ...styles.wrapping}}>
        {properties.tags.map((it, index) => {
          return (
            <A key={index} href={PHONEBOOK.WEBSITE + "/tags/" + it} style={categoryStyle}><P
              style={styles.fitting}>{it}</P></A>
          )
        })}
      </div>
    </Card>
  );
}

function pickCategory(from) {
  switch (from) {
    case "all":
      return strings.browse
    case "art":
      return strings.art
    case "music":
      return strings.music
    case "gaming":
      return strings.gaming
    case "irl":
      return strings.irl
    default:
      return strings.browse
  }
}

const styles = {
  wrapping: {
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  preview: {
    width: "25vw",
    height: "auto",
  },
  categories: {
    borderStyle: "solid",
    borderRadius: "15px",
    borderWidth: "1px",
    padding: "2px",
    marginRight: "1%"
  },
  separated: {
    marginRight: "2vw",
    marginLeft: "2vw",
    marginTop: "3%",
    padding: "0.4%"
  },
  fitting: {
    paddingLeft: "0.6em",
    paddingRight: "0.6em"
  },
  clickable: {
    cursor: "pointer"
  }
}

const strings = new LocalizedStrings({
  en: {
    browse: "All live streaming",
    art: "Art live streaming",
    music: "Music live streaming",
    gaming: "Gaming live streaming",
    irl: "IRL live streaming",
  },
  it: {
    browse: "Tutte le dirette",
    art: "Dirette artistiche",
    music: "Dirette musicali",
    gaming: "Dirette gaming",
    irl: "Dirette IRL",
  },
  ru: {
    browse: "",
    art: "",
    music: "",
    gaming: "",
    irl: "",
  }
});

export default StreamList;