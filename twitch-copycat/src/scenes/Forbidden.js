import forbidden from "assets/images/forbidden.webp";
import Column from "components/Column";
import Figure from "components/Figure";
import H from "components/H";
import React from 'react';
import LocalizedStrings from "react-localization";

function Forbidden({user}) {
  if (user) {
    return (
      <Column>
        <H number={1}>{strings.forbiddenMessage}</H>
        <Figure src={forbidden} alt={strings.forbidden}/>
      </Column>
    );
  } else {
    return (
      <Column>
        <H number={1}>{strings.forbiddenMessage}</H>
        <Figure src={forbidden} alt={strings.forbidden}/>
      </Column>
    );
  }
}

const strings = new LocalizedStrings({
  en: {
    forbidden: "503 - Forbidden",
    forbiddenMessage: "This page is for users only.",
    notUserOnly: "This page is for not users only. Logout before continuing"
  },
  it: {
    forbidden: "503 - Forbidden",
    forbiddenMessage: "Questa pagina è riservata agli utenti",
    notUserOnly: "Questa pagina è riservata ai non utenti. Effettua il logout prima di continuare."
  },
  ru: {
    forbidden: "",
    forbiddenMessage: "",
    notUserOnly: ""
  }
});

export default Forbidden;