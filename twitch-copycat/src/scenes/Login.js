import {Paper, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import A from "components/A";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import {UserContext, UserManipulationContext} from "contexts/UserContext";
import React, {useContext, useEffect, useState} from 'react';
import LocalizedStrings from "react-localization";
import {useHistory} from "react-router-dom";
import Forbidden from "scenes/Forbidden";
import {appendStyle, common} from "styles/common";
import LockIcon from '@material-ui/icons/Lock';
import PersonIcon from '@material-ui/icons/Person';
import {checkValidEmail, checkValidPassword, checkValidUser, digestSha256, PHONEBOOK} from "utils/common";

function Login() {
  useLanguage(strings)
  const theme = useTheme()
  const loggedUser = useContext(UserContext)
  const [user, setUser] = useState("")
  const [validUser, setValidUser] = useState(undefined)
  const [errorUser, setErrorUser] = useState(false)
  const [profileSwitch, setProfileSwitch] = useState(false)
  const [password, setPassword] = useState("")
  const [validPassword, setValidPassword] = useState(undefined)
  const [errorPassword, setErrorPassword] = useState(false)
  const [errorLogin, setErrorLogin] = useState(false)
  const userSetter = useContext(UserManipulationContext)
  const history = useHistory()

  useEffect(() => {
    setValidUser(checkValidUser(user))
  }, [user])

  useEffect(() => {
    if (profileSwitch) {
      history.push("/profile")
    }
  }, [profileSwitch])

  useEffect(() => {
    setValidPassword(checkValidPassword(password))
  }, [password])

  function normalizeData() {
    if (user === undefined) {
      setValidUser(checkValidUser(""))
    } else {
      setUser(current => current.trim())
    }
    if (password === undefined) {
      setValidPassword(checkValidPassword(""))
    }
  }

  function checkAll() {
    if (!validUser) {
      setErrorUser(true)
    }
    if (!validPassword) {
      setErrorPassword(true)
    }
    return validUser && validPassword
  }

  const errorStyle = appendStyle(common.zeroed, "color", "red")

  async function requestAccess() {
    if (checkAll()) {
      normalizeData()
      const digest = digestSha256(password)

      fetch(PHONEBOOK.EXPRESS + "/api/login", {
        method: "post",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
          method: checkValidEmail(user) ? "mail" : "username",
          user: user,
          password: digest
        })
      })
        .then(res => res.text())
        .then(JSON.parse)
        .then(it => {
          if (it.found === false) {
            setErrorLogin(true)
            setErrorUser(true)
            setErrorPassword(true)
          } else {
            userSetter(it)
            setProfileSwitch(true)
            setErrorLogin(false)
          }
        })
        .catch(it => console.log("ERROR: " + it))
    }
  }

  if (loggedUser !== undefined) {
    return (<Forbidden user={true}/>)
  } else {
    return (
      <section style={styles.section}>
        <div>
          <P>{strings.login}</P>
        </div>
        <Paper style={styles.paper}>
          <div>
            <div style={styles.aligned}>
              <TextField
                label={strings.userLabel}
                value={user}
                onChange={(event) => {
                  setUser(event.target.value)
                }}
                variant="outlined"
                autoFocus
                required
                error={!validUser && errorUser}
              />
              <PersonIcon/>
            </div>
          </div>
          <div>
            <div style={styles.aligned}>
              <TextField
                label={strings.passwordLabel}
                value={password}
                onChange={(event) => {
                  setPassword(event.target.value)
                }}
                type="password"
                autoComplete="current-password"
                variant="outlined"
                required
                error={!validPassword && errorPassword}
              />
              <LockIcon/>
            </div>
          </div>
          <A href={PHONEBOOK.WEBSITE + "/recovery"} style={styles.forgotten}>{strings.forgotten}</A>
          <Button onClick={() => requestAccess()} fullWidth
                  style={appendStyle(styles.button, "backgroundColor", theme.dilutedSecondaryColor)}>
            <P>{strings.loginMessage}</P>
          </Button>
          <A href={PHONEBOOK.WEBSITE + "/login"} style={styles.forgotten}>{strings.register}</A>
        </Paper>
        <div style={styles.errorColumn}>
          <p style={errorStyle}>{errorLogin ? strings.userNotFound : ""}</p>
          <p style={errorStyle}>{errorLogin ? strings.checkInfo : ""}</p>
        </div>
      </section>
    );

  }
}

const strings = new LocalizedStrings({
  en: {
    forgotten: "Forgot your password?",
    loginMessage: "Login",
    register: "Need to register?",
    userLabel: "Username or email",
    passwordLabel: "Password",
    userNotFound: "Could not log in the specified user.",
    checkInfo: "Check your info and password."

  },
  it: {
    forgotten: "Password dimenticata?",
    loginMessage: "Accedi",
    register: "Devi registrarti?",
    userLabel: "Username o email",
    passwordLabel: "Password",
    userNotFound: "Non si è potuto procedere al login.",
    checkInfo: "Controllare nome utente e password."

  },
  ru: {
    forgotten: "Пароль забыта? ",
    loginMessage: "Авторизоваться",
    register: "Нужно зарегистрироваться?",
    userLabel: "Имя пользователя или Эл. адрес",
    passwordLabel: "Пароль",
    userNotFound: ""
  }
});

const styles = {
  section: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignContent: "center"
  },
  forgotten: {
    textAlign: "right",
    fontSize: "0.75em"
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    paddingTop: "6%",
    paddingBottom: "6%",
    paddingLeft: "3%",
    paddingRight: "3%",
    backgroundColor: "transparent"
  },
  aligned: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    marginTop: "3%"
  },
  errorColumn: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
    marginTop: "3%"
  },
  row: {
    display: "flex",
    flexDirection: "row",
    marginTop: "5%",
    fontSize: "0.9em",
    wordWrap: "none"
  },
  button: {
    marginTop: "10%"
  }
}

export default Login;