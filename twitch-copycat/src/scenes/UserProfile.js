import Button from "@material-ui/core/Button";
import Column from "components/Column";
import H from "components/H";
import P from "components/P";
import Row from "components/Row";
import {UserContext} from "contexts/UserContext";
import React, {useContext} from 'react';
import LocalizedStrings from "react-localization";
import {useHistory} from "react-router-dom";
import Forbidden from "scenes/Forbidden";

function UserProfile() {
  const loggedUser = useContext(UserContext)

  if (loggedUser === undefined) {
    return (
      <Forbidden logged={false}/>
    )
  } else {
    return (
      <Column>
        <H number={2}>{strings.info}</H>
        <UserInfo info={loggedUser.info[0]}/>
        <H number={2}>{strings.statistics}</H>
        <Statistics info={loggedUser.info[0]}/>
        <H number={2}>{strings.userSettings}</H>
        <UserSettings/>
      </Column>
    )
  }
}

function UserInfo({info}) {
  return (
    <Column>
      <P>{strings.userLabel + ": " + info.username}</P>
      <P>{strings.mailLabel + ": " + info.email}</P>
    </Column>
  )
}

function Statistics({info}) {

  function formatDate(date) {
    const actualDate = new Date(date)
    const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
    return actualDate.toLocaleDateString("en-US", options)
  }

  return (
    <Column>
      <Column>
        <Statistic stat={strings.goLiveLabel} route={"/broadcast"}>{}</Statistic>
        <Statistic stat={strings.userSinceLabel}>{formatDate(info.signupDate)}</Statistic>
        <Statistic stat={strings.replayStatLabel} route={"/"}>{info.completedStreams}</Statistic>
        <Statistic stat={strings.followersLabel} route={"/"}>{info.followers.length}</Statistic>
        <Statistic stat={strings.followingLabel} route={"/"}>{info.following.length}</Statistic>
      </Column>
    </Column>
  )
}

function Statistic({stat, route, children}) {
  const history = useHistory()

  if (stat === strings.userSinceLabel) {
    return (
      <Row>
        <P>{stat + ": " + children}</P>
      </Row>
    )
  }
  if (stat === strings.goLiveLabel) {
    return (
      <Row>
        <P>{""}</P>
        <Button onClick={() => history.push(route)}>{stat}</Button>
      </Row>
    )
  } else {
    return (
      <Row>
        <P>{stat + ": " + children}</P>
        <Button onClick={() => history.push(route)}>{strings.manageLabel}</Button>
      </Row>
    )
  }
}

function UserSettings() {
  return (
    <Column>
      <Button>{strings.changeSettings}</Button>
    </Column>
  )
}

const strings = new LocalizedStrings({
    en: {
      info: "User's info",
      userLabel: "Username",
      nameLabel: "Name",
      surnameLabel: "Surname",
      phoneLabel: "Phone",
      mailLabel: "Mail address",
      statistics: "User's statistics",
      userSinceLabel: "User since",
      streamsStatLabel: "# of streams",
      goLiveLabel: "Go live",
      replayStatLabel: "# of replay",
      manageLabel: "Manage",
      followersLabel: "Followers",
      followingLabel: "Following",
      modifyLabel: "Change",
      deleteUser: "Delete user",
      profilePicLabel: "User's pic",
      currentBioLabel: "User's biography",
      userSettings: "User's settings",
      changeSettings: "Change settings",

    },
    it: {
      info: "Info utente",
      userLabel: "Nome utente",
      nameLabel: "Nome",
      surnameLabel: "Cognome",
      phoneLabel: "Telefono",
      mailLabel: "Indirizzo mail",
      statistics: "Statistiche",
      userSinceLabel: "Utente da: ",
      streamsStatLabel: "Numero di stream: ",
      goLiveLabel: "Vai live",
      replayStatLabel: "Numero di replay: ",
      manageLabel: "Gestisci",
      followersLabel: "Seguito da: ",
      followingLabel: "Seguiti da te: ",
      modifyLabel: "Modifica",
      deleteUser: "Cancella utente",
      profilePicLabel: "Immagine utente",
      currentBioLabel: "Bio utente",
      userSettings: "Impostazioni utente",
      changeSettings: "Cambia impostazioni",

    },
    ru: {
      info: "",
      userLabel: "",
      nameLabel: "",
      surnameLabel: "",
      phoneLabel: "",
      mailLabel: "",
      statistics: "",
      userSinceLabel: "",
      streamsStatLabel: "",
      goLiveLabel: "",
      replayStatLabel: "",
      manageLabel: "",
      followersLabel: "",
      followingLabel: "",
      modifyLabel: "",
      deleteUser: "",
      profilePicLabel: "",
      currentBioLabel: "",
      userSettings: "",
      changeSettings: "",

    },
  }
);


export default UserProfile;