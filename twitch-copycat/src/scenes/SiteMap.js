import React from 'react';
import A from "components/A";
import H from "components/H";
import {useLanguage} from "contexts/LanguageContext";
import LocalizedStrings from "react-localization";
import {PHONEBOOK} from "../utils/common";

function SiteMap() {
  useLanguage(strings)

  return (
    <section>
      <H number={1}>{strings.sitemap}</H>
      <ul>
        <li><A href={PHONEBOOK.WEBSITE}>{strings.home}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/streams"}>{strings.browse}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/categories"}>{strings.categories}</A></li>
        <ul>
          <li><A href={PHONEBOOK.WEBSITE + "/streams/irl"}>{strings.irl}</A></li>
          <li><A href={PHONEBOOK.WEBSITE + "/streams/gaming"}>{strings.gaming}</A></li>
          <li><A href={PHONEBOOK.WEBSITE + "/streams/art"}>{strings.art}</A></li>
          <li><A href={PHONEBOOK.WEBSITE + "/streams/music"}>{strings.music}</A></li>
        </ul>
        <li><A href={PHONEBOOK.WEBSITE + "/broadcast"}>{strings.broadcast}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/tos"}>{strings.tos}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/faq"}>{strings.faq}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/feedback"}>{strings.feedback}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/credits"}>{strings.credits}</A></li>
        <li><A href={PHONEBOOK.WEBSITE + "/sitemap"}>{strings.sitemap}</A></li>
      </ul>
    </section>
  );
}

const strings = new LocalizedStrings({
  en: {
    sitemap: "Sitemap",
    home: "Homepage",
    browse: "Browse",
    categories: "Categories",
    irl: "IRL",
    gaming: "Gaming",
    art: "Art",
    music: "Music",
    broadcast: "Broadcast",
    tos: "T.O.S.",
    privacy: "Privacy",
    credits: "Credits",
    faq: "F.A.Q.",
    feedback: "Feedback",
  },
  it: {
    sitemap: "Mappa del sito",
    home: "Pagina iniziale",
    browse: "Esplora",
    categories: "Categorie",
    irl: "IRL",
    gaming: "Gaming",
    art: "Arte",
    music: "Musica",
    broadcast: "Trasmetti",
    tos: "Termini di Servizio",
    privacy: "Privacy",
    credits: "Ringraziamenti",
    faq: "F.A.Q.",
    feedback: "Feedback",
  },
  ru: {
    sitemap: "",
    home: "",
    browse: "",
    categories: "",
    irl: "",
    gaming: "",
    art: "",
    music: "",
    broadcast: "",
    tos: "",
    privacy: "",
    credits: "",
    faq: "",
    feedback: "",
  }
});

export default SiteMap;