import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React, {useContext, useEffect, useState} from 'react';
import LocalizedStrings from "react-localization";
import {useRecordWebcam} from 'react-record-webcam'
import Forbidden from "./Forbidden";
import Column from "../components/Column";
import {UserContext} from "../contexts/UserContext";
import Row from "../components/Row";
import {FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, TextField} from "@material-ui/core";
import {PHONEBOOK} from "../utils/common";

function Broadcast() {
  const loggedUser = useContext(UserContext)
  useLanguage(strings)
  const theme = useTheme()

  const [title, setTitle] = useState("")
  const [category, setCategory] = useState("art")
  const [tags, setTags] = useState("")
  const [live, setLive] = useState(false)
  const recordWebcam = useRecordWebcam();

  useEffect(() => {
    recordWebcam.open()
  }, [])

  function startLiveStreaming() {
    fetch(PHONEBOOK.EXPRESS + "/api/startLive", {
      method: "post",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        streamer: loggedUser.info[0].username,
        title: title,
        category: category,
        tags: tags.split(",").map(s => s.trim()),
        viewers: 1
      })
    })
      .then(res => res.text())
      .then(JSON.parse)
      .then(it => setLive(it.started))
      .catch(it => console.log("ERROR: " + it))
  }

  function stopLiveStreaming() {
    fetch(PHONEBOOK.EXPRESS + "/api/stopLive", {
      method: "post",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        username: loggedUser.info[0].username,
      })
    })
      .then(res => res.text())
      .then(JSON.parse)
      .then(it => setLive(it.started))
      .catch(it => console.log("ERROR: " + it))
  }

  if (loggedUser === undefined) {
    return (
      <Forbidden logged={false}/>
    )
  } else {
    return (
      <div>
        <Row>
          <Column>
            <Column>
              <p>Camera Status: {recordWebcam.status}</p>
              <p>Live Status: {live ? "LIVE" : "NOT LIVE"}</p>
              <TextField label={strings.title}
                         style={styles.row}
                         required
                         onChange={(event) => {
                           setTitle(event.target.value)
                         }}
                         value={title}
                         variant="outlined"/>
              <TextField label={strings.tagsLabel}
                         style={styles.row}
                         required
                         onChange={(event) => {
                           setTags(event.target.value)
                         }}
                         value={tags}
                         variant="outlined"/>
              <FormControl>
                <FormLabel id="formlabelid">{strings.category}</FormLabel>
                <RadioGroup
                  aria-labelledby="demo-radio-buttons-group-label"
                  defaultValue="art"
                  name="radio-buttons-group"
                  onChange={(event) => setCategory(event.target.value)}
                >
                  <FormControlLabel value="art" control={<Radio/>} label={strings.art}/>
                  <FormControlLabel value="music" control={<Radio/>} label={strings.music}/>
                  <FormControlLabel value="gaming" control={<Radio/>} label={strings.gaming}/>
                  <FormControlLabel value="irl" control={<Radio/>} label={strings.irl}/>
                </RadioGroup>
              </FormControl>
              <button disabled={live} onClick={startLiveStreaming}>Start LIVE</button>
              <button disabled={!live} onClick={stopLiveStreaming}>Stop LIVE</button>
            </Column>
          </Column>
          <Column>
            <video ref={recordWebcam.webcamRef} autoPlay muted/>
          </Column>
        </Row>
      </div>
    )
  }
}

const styles = {
  row: {
    marginTop: "2%",
    fontSize: "0.9em",
    wordWrap: "none"
  }
}

const strings = new LocalizedStrings({
  en: {
    title: "Title",
    category: "Category",
    tags: "Tags",
    tagsLabel: "Tags (comma separated)",
    art: "Art",
    music: "Music",
    gaming: "Gaming",
    irl: "IRL",
  },
  it: {
    title: "Titolo",
    category: "Categoria",
    tags: "Etichette",
    tagsLabel: "Etichette (separate da virgola)",
    art: "Arte",
    music: "Musica",
    gaming: "Videogiochi",
    irl: "IRL",
  },
  ru: {
    title: "",
    category: "",
    tags: "",
    tagsLabel: "",
    art: "",
    music: "",
    gaming: "",
    irl: "",
  }
});

export default Broadcast;