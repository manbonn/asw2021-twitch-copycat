import Stream from "components/stream/Stream";
import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import Missing from "scenes/Missing";
import {PHONEBOOK} from "utils/common";

function UserStream() {
  const {user} = useParams();
  const [liveFetched, setLiveFetched] = useState(undefined)
  const [userFetched, setUserFetched] = useState(undefined)

  useEffect(() => {

    fetch(PHONEBOOK.EXPRESS + "/api/user/" + user, {
      method: "get",
      headers: {"Content-Type": "application/json"},
    })
      .then(res => res.text())
      .then(JSON.parse)
      .then(it => {
        setUserFetched(it)
      })
      .catch(it => console.log("ERROR: " + it))

    fetch(PHONEBOOK.EXPRESS + "/api/stream/" + user, {
      method: "get",
      headers: {"Content-Type": "application/json"},
    })
      .then(res => res.text())
      .then(JSON.parse)
      .then(it => {
        setLiveFetched(it)
      })
      .catch(it => console.log("ERROR: " + it))

  }, [])

  if (userFetched === undefined || liveFetched === undefined) {
    return (
      <Missing/>
    )
  } else {
    if (userFetched.online) {
      return (
        <Stream
          username={user}
          liveSource={liveFetched.mediaSource}
          bio={userFetched.bio}
          followers={userFetched.followers}
          title={liveFetched.title}
          category={liveFetched.category}
          viewers={liveFetched.viewers}
          tags={liveFetched.tags}
        />
      )
    } else {
      return (
        <Stream
          username={user}
          liveSource={liveFetched.liveSource}
          followers={userFetched.followers}
          title={""}
          category={""}
          viewers={0}
          tags={[]}
        />
      )
    }
  }


}

export default UserStream;