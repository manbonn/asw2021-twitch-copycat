import H from "components/H";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import React from 'react';
import LocalizedStrings from "react-localization";

function Faq() {
  useLanguage(strings)

  return (
    <section style={styles.section}>
      <H number={1}>{strings.faq}</H>
      <Corpus question={strings.aswCharmanderQ} answer={strings.aswCharmanderA}/>
      <Corpus question={strings.charmanderQ} answer={strings.charmanderA}/>
      <Corpus question={strings.moneyQ} answer={strings.moneyA}/>
      <Corpus question={strings.paymentQ} answer={strings.paymentA}/>
      <Corpus question={strings.informationQ} answer={strings.informationA}/>
      <Corpus question={strings.apiQ} answer={strings.apiA}/>
      <Corpus question={strings.askingQ} answer={strings.askingA}/>
      <Corpus question={strings.forMeQ} answer={strings.forMeA}/>
      <Corpus question={strings.teachQ} answer={strings.teachA}/>
    </section>
  );
}

function Corpus({question, answer}) {
  return (
    <div style={styles.complete}>
      <P style={styles.question}>{"Q: " + question}</P>
      <P style={styles.answer}>{"A: " + answer}</P>
    </div>
  );
}

const styles = {
  section: {
    marginLeft: "13vw",
    marginRight: "13vw",
    textJustify: "auto",
    marginBottom: "20px",
  },
  complete: {
    marginLeft: "2vw",
    marginBottom: "2vw"
  },
  question: {
    fontWeight: "550"
  },
  answer: {
    fontWeight: "350"
  }
}

const strings = new LocalizedStrings({
  en: {
    faq: "Frequently Asked Questions",
    charmanderQ: "Why Charmander?",
    charmanderA: "Once I understood I had to remember the username to logon on my first remote server, I chose a name I could not forget. That server evolved a lot since then, and the web application it is hosting now is finally its very own face. Furthermore, Charmander is the coolest.",
    aswCharmanderQ: "What is this website?",
    aswCharmanderA: "It is the final project for a course I attended at the University of Bologna, called Web Services and Applications. A.S.W. is the acronym from the course, and I maintained it into the website name.",
    moneyQ: "Are you making money/Do you plan to make money through this website?",
    moneyA: "Not a single dime. This website is only intended to be a final project.",
    paymentQ: "Wait, but there is a payment option to buy stuff all over the website!",
    paymentA: "That's a fake A.P.I provided by the main card handlers so that I can learn how to manage actual payment.",
    apiQ: "What's an A.P.I.?",
    apiA: "If you have to ask, you’ll never know. If you know, you only need to call.",
    informationQ: "Are you stealing my information?",
    informationA: "I won't even know what to do with them.",
    askingQ: "Are this questions actually that frequent?",
    askingA: "Maybe.",
    forMeQ: "Will you write a website for me?",
    forMeA: "No.",
    teachQ: "Will you teach me how to write my own website?",
    teachA: "No.",
  },
  it: {
    faq: "Frequently Asked Questions",
    charmanderQ: "Perchè Charmander?",
    charmanderA: "Una volta capito che mi sarei dovuto ricordare l'username per loggare sul mio primo server remoto, scelsi un nome che non mi sarei potuto dimenticare. Quel server si è evoluto molto da allora, e l'applicazione web che ospita gli ha finalmente dato un volto. Oltretutto, Charmander è il più fico.",
    aswCharmanderQ: "Cos'è questo sito?",
    aswCharmanderA: "Il progetto finale di un corso che ho frequentato all'università di Bologna, chiamato Applicazioni e Servizi Web. A.S.W. ne è l'acronimo, e l'ho mantenuto nel nome del sito.",
    moneyQ: "Fai/Farai soldi attraverso il sito?",
    moneyA: "Neanche un centesimo. Il sito è inteso solo come progetto finale.",
    paymentQ: "Spè ma ci sono opzioni di pagamento per comprare cose sparse per l'intero sito!",
    paymentA: "Sono A.P.I. finte messe a disposizione dai gestori di carte di credito, così che possa imparare a gestire veri pagamenti.",
    apiQ: "Cos'è un A.P.I.?",
    apiA: "Se devi chiedere non lo saprai mai, se lo sai devi solo chiamare.",
    informationQ: "Stai rubando le mie informazioni?",
    informationA: "Non saprei nemmeno cosa farci.",
    askingQ: "Queste domande sono davvero così frequenti?",
    askingA: "Forse",
    forMeQ: "Mi fai un sito?",
    forMeA: "No",
    teachQ: "Mi insegni a fare un sito?",
    teachA: "No",
  },
  ru: {
    faq: "",
    charmanderQ: "",
    charmanderA: "",
    aswCharmanderQ: "",
    aswCharmanderA: "",
    moneyQ: "",
    moneyA: "",
    paymentQ: "",
    paymentA: "",
    apiQ: "",
    apiA: "",
    informationQ: "",
    informationA: "",
    askingQ: "",
    askingA: "",
    forMeQ: "",
    forMeA: "",
    teachQ: "",
    teachA: "",
  }
});

export default Faq;