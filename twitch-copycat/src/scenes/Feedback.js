import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import React from 'react';
import LocalizedStrings from "react-localization";

function Feedback() {
  useLanguage(strings)

  return (
    <section>
      <P>{strings.feedback}</P>
    </section>
  );
}

const strings = new LocalizedStrings({
  en: {
    feedback: "Through this page will pe possible to send feedback about the website.",
  },
  it: {
    feedback: "Questa pagina permetterà di inviare feedback relativi al sito.",
  },
  ru: {
    feedback: "",
  }
});

export default Feedback;