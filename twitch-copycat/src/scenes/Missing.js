import H from "components/H";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import React from 'react';
import missing from 'assets/images/missing.png'
import LocalizedStrings from "react-localization";

function Missing() {
  useLanguage(strings)

  return (
    <div style={styles.missing}>
      <H number={1}>{strings.title}</H>
      <P style={styles.missingText}>{strings.message}</P>
      <br/>
      <img src={missing} alt={"Charizard looking for the 404 content."} style={styles.missingImage}/>
    </div>
  );
}

const styles = {
  missing: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  missingImage: {
    width: "45vw",
    height: "auto"
  },
  missingText: {
    marginBottom: "10px"
  }
}

const strings = new LocalizedStrings({
  en: {
    title: "The content you are looking for might be lost in the forest.",
    message: "Charmander is looking for it, but it does not seem to exist.",
  },
  it: {
    title: "Il contenuto richiesto potrebbe essersi smarrito nella foresta.",
    message: "Charmander lo sta cercando, ma non sembra esistere.",
  },
  ru: {
    title: "То, что вы ищете, может затеряться в лесу.",
    message: "Чармандер ищет его, но, похоже, его не существует. ",
  }
});

export default Missing;