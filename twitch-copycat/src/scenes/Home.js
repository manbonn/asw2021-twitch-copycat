import SpinningCharmander from "components/SpinningCharmander";
import React from 'react';

function Home() {
  return (
    <section>
      <SpinningCharmander/>
    </section>
  );
}

export default Home;