import P from "components/P";
import React from 'react';

function SearchResult({username, viewers, online}) {

  function isOnline() {
    return online ? "✔" : "❌"
  }

  return (
    <div style={styles.row}>
      <P style={styles.separated}>{username}</P>
      <P style={styles.separated}>{viewers}</P>
      <P style={styles.rightAligned}>{isOnline()}</P>
    </div>
  );
}

const styles = {
  growPopper: {},
  row: {
    display: "flex",
    flexDirection: "row",
  },
  separated: {
    color: "white"
  },
  rightAligned: {
    textAlign: "right"
  }
}

export default SearchResult;