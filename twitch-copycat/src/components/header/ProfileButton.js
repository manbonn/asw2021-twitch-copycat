import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import P from "components/P";
import {LanguageManipulationContext, useLanguage} from "contexts/LanguageContext";
import {ThemeContext, ThemeManipulationContext, useTheme} from "contexts/ThemeContext";
import React, {useContext, useRef, useState} from 'react';
import 'styles/header-rwd.css';
import Divider from '@material-ui/core/Divider';

import ukFlag from "assets/images/uk.png"
import itaFlag from "assets/images/ita.png"
import ruFlag from "assets/images/ru.png"


import profilePicOriginal from 'assets/images/profile-original.png'
import profilePicDark from 'assets/images/profile-dark.png'
import LocalizedStrings from "react-localization";
import {useHistory} from "react-router-dom";
import {appendStyle} from "styles/common";


function ProfileButton() {
  const [openProfilePopper, setOpenProfilePopper] = useState(false);
  const [openLanguagePopper, setOpenLanguagePopper] = useState(false);
  const [openThemePopper, setOpenThemePopper] = useState(false);

  const profileRef = useRef(null);
  const languageRef = useRef(null);
  const themeRef = useRef(null);
  useLanguage(strings)
  const history = useHistory()
  const theme = useTheme()
  const themeName = useContext(ThemeContext)

  function switchProfile(theme) {
    switch (theme) {
      case "original":
        return <img className={"profileImg"} src={profilePicOriginal} alt="User profile pic"/>
      case "dark":
        return <img className={"profileImg"} src={profilePicDark} alt="User profile pic"/>
      case "tritanopia":
        return <img className={"profileImg"} src={profilePicDark} alt="User profile pic"/>
      case "protanopia":
        return <img className={"profileImg"} src={profilePicDark} alt="User profile pic"/>
      case "deuteranopia":
        return <img className={"profileImg"} src={profilePicDark} alt="User profile pic"/>
      default:
        return <img className={"profileImg"} src={profilePicOriginal} alt="User profile pic"/>
    }
  }

  return (
    <div className={"profilePic"}>
      <Button ref={profileRef} onClick={() => togglePopper()}>
        {switchProfile(themeName)}
      </Button>
      <Popper open={openProfilePopper} anchorEl={profileRef.current} role={undefined} transition>
        {({TransitionProps, placement}) => (
          <Grow {...TransitionProps} style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}>
            <Paper style={appendStyle(styles.growPopper, "backgroundColor", theme.dilutedSecondaryColor)}>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList autoFocusItem={openProfilePopper} id="menu-list-grow">
                  <MenuItem ref={languageRef} onClick={toggleLanguagePopper}>
                    {strings.language}
                    <Popper open={openLanguagePopper} anchorEl={languageRef.current} placement={"left-start"}
                            role={undefined} transition>
                      {({TransitionProps}) => (
                        <Grow {...TransitionProps}>
                          <Paper style={appendStyle(styles.growPopper, "backgroundColor", theme.dilutedSecondaryColor)}>
                            <ClickAwayListener onClickAway={handleClose}>
                              <LanguageManipulationContext.Consumer>
                                {setLanguage =>
                                  <MenuList autoFocusItem={openLanguagePopper} id="menu-list-grow">
                                    <MenuItem onClick={() => setLanguage("en")}>
                                      <div style={styles.row}>
                                        <img style={styles.iconFlag} src={ukFlag} alt={"flag icon"}/>
                                        <P>English</P>
                                      </div>
                                    </MenuItem>
                                    <MenuItem onClick={() => setLanguage("it")}>
                                      <div style={styles.row}>
                                        <img style={styles.iconFlag} src={itaFlag} alt={"flag icon"}/>
                                        <P>Italiano</P>
                                      </div>
                                    </MenuItem>
                                    <MenuItem onClick={() => setLanguage("ru")}>
                                      <div style={styles.row}>
                                        <img style={styles.iconFlag} src={ruFlag} alt={"flag icon"}/>
                                        <P>Ру́сский</P>
                                      </div>
                                    </MenuItem>
                                  </MenuList>
                                }
                              </LanguageManipulationContext.Consumer>
                            </ClickAwayListener>
                          </Paper>
                        </Grow>
                      )}
                    </Popper>
                  </MenuItem>
                  <MenuItem ref={themeRef} onClick={toggleThemePopper}>
                    {strings.theme}
                    <Popper open={openThemePopper} anchorEl={themeRef.current} placement={"left-start"} role={undefined}
                            transition>
                      {({TransitionProps}) => (
                        <Grow {...TransitionProps}>
                          <Paper style={appendStyle(styles.growPopper, "backgroundColor", theme.dilutedSecondaryColor)}>
                            <ClickAwayListener onClickAway={handleClose}>
                              <ThemeManipulationContext.Consumer>
                                {setTheme =>
                                  <MenuList autoFocusItem={openThemePopper} id="menu-list-grow">
                                    <MenuItem onClick={() => setTheme("original")}>{strings.original}</MenuItem>
                                    <MenuItem onClick={() => setTheme("dark")}>{strings.dark}</MenuItem>
                                    <MenuItem onClick={() => setTheme("protanopia")}>{strings.protanopia}</MenuItem>
                                    <MenuItem onClick={() => setTheme("tritanopia")}>{strings.tritanopia}</MenuItem>
                                    <MenuItem onClick={() => setTheme("deuteranopia")}>{strings.deuteranopia}</MenuItem>
                                  </MenuList>
                                }
                              </ThemeManipulationContext.Consumer>
                            </ClickAwayListener>
                          </Paper>
                        </Grow>
                      )}
                    </Popper>
                  </MenuItem>
                  <Divider orientation={"horizontal"}/>
                  <MenuItem onClick={() => history.push("/login")}>
                    {strings.login}
                  </MenuItem>
                  <MenuItem onClick={() => history.push("/signup")}>
                    {strings.signup}
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );

  function handleClose(event) {
    const onProfilePopper = profileRef.current && profileRef.current.contains(event.target)
    const onLanguagePopper = languageRef.current && languageRef.current.contains(event.target)
    const onThemePopper = themeRef.current && themeRef.current.contains(event.target)

    if (onProfilePopper || onLanguagePopper || onThemePopper) {
      return;
    }

    setOpenProfilePopper(false);
    setOpenLanguagePopper(false);
    setOpenThemePopper(false);
  }

  function toggleThemePopper() {
    setOpenLanguagePopper(false)
    setOpenThemePopper((prevOpen) => !prevOpen)
  }

  function toggleLanguagePopper() {
    setOpenThemePopper(false)
    setOpenLanguagePopper((prevOpen) => !prevOpen)
  }

  function togglePopper() {
    setOpenThemePopper(false)
    setOpenLanguagePopper(false)
    setOpenProfilePopper((prevOpen) => !prevOpen);
  }
}

const styles = {
  growPopper: {},
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  iconFlag: {
    width: "auto",
    height: "1em",
    marginRight: "10px",
  }
}

const strings = new LocalizedStrings({
  en: {
    language: "Language",
    theme: "Theme",
    login: "Login",
    signup: "Signup",
    original: "Original",
    dark: "Dark",
    deuteranopia: "Deuteranopia",
    protanopia: "Protanopia",
    tritanopia: "Tritanopia",
  },
  it: {
    language: "Lingua",
    theme: "Tema",
    login: "Login",
    signup: "Registrazione",
    original: "Originale",
    dark: "Scuro",
    deuteranopia: "Deuteranopia",
    protanopia: "Protanopia",
    tritanopia: "Tritanopia",
  },
  ru: {
    language: "Уazyk",
    theme: "Палитра",
    login: "Логин",
    signup: "Подпишитесь",
    original: "Оригинал",
    dark: "Темный",
    deuteranopia: "Дейтеранопия",
    protanopia: "Протанопия",
    tritanopia: "Тританопия",
  }
});

export default ProfileButton;