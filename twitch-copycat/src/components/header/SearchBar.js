import {InputBase} from "@material-ui/core";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import SearchIcon from "@material-ui/icons/Search";
import SearchResult from "components/header/SearchResult";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React, {useEffect, useRef, useState} from 'react';
import LocalizedStrings from "react-localization";
import {useHistory} from "react-router-dom";
import {appendStyle, common} from "styles/common";
import {onEnter, searchQuery} from "utils/common";

function SearchBar() {
  useLanguage(strings)
  const theme = useTheme()
  const searchBarRef = useRef(null)
  const [search, setSearch] = useState(undefined)
  const [result, setResult] = useState([])
  const [openResult, setOpenResult] = useState(false)
  const history = useHistory()

  useEffect(() => {
    if (search !== undefined) {
      searchQuery(search).then(JSON.parse).then(setResult)
    }
  }, [search])

  useEffect(() => {
    if (result) {
      setOpenResult(true)
    } else {
      setOpenResult(false)
    }
  }, [result])

  const onChange = (event) => {
    setSearch(event.target.value);
    console.log(event.target.value);
    if (event.target.value === "") {
      setOpenResult(false)
    }
  };

  const onSubmit = e => {
    e.preventDefault();
    searchQuery(search).then(JSON.parse).then(setResult)
  };

  function handleClose(event) {
    const onSearchBarPopper = searchBarRef.current && searchBarRef.current.contains(event.target)

    if (onSearchBarPopper) {
      return;
    }

    setOpenResult(false);
  }

  function handleClick(username) {
    setOpenResult(false)
    history.push("/user/" + username)
  }

  return (
    <div className={"searchbar"} style={appendStyle(styles.searchbar, "color", theme.primaryText)}>
      <form onSubmit={onSubmit}>
        <InputBase
          style={appendStyle(common.empty, "color", theme.primaryText)}
          placeholder={strings.search}
          value={search}
          onKeyPress={(event) => onEnter(event)}
          onChange={onChange}
          ref={searchBarRef}
        />
        <Popper open={openResult} anchorEl={searchBarRef.current} transition>
          {({TransitionProps, placement}) => (
            <Grow {...TransitionProps}
                  style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}>
              <Paper style={appendStyle(styles.growPopper, "backgroundColor", theme.dilutedSecondaryColor)}>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList id="menu-list-grow">
                    {result.map((element) => {
                      return (
                        <MenuItem onClick={() => handleClick(element.username)}>
                          <SearchResult
                            username={element.username}
                            viewers={element.viewers}
                            online={element.online}
                          />
                        </MenuItem>
                      )
                    })}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </form>
      <SearchIcon/>
    </div>
  );
}

const styles = {
  searchbar: {
    border: "2px",
    borderRadius: "12px",
    borderStyle: "solid",

    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "right"
  },
}

const strings = new LocalizedStrings({
  en: {
    search: "Search..."
  },
  it: {
    search: "Cerca..."
  },
  ru: {
    search: "Искайте..."
  }
});

export default SearchBar;