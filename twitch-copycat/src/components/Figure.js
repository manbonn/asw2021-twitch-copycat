import React from 'react';
import {common} from "styles/common";

function Figure({src, alt, style, caption}) {
  const actualStyle = {...common.zeroed, ...style}

  if (caption) {
    return (
      <figure style={common.zeroed}>
        <img src={src} alt={alt} style={actualStyle}/>
        <figcaption>{caption}</figcaption>
      </figure>
    );
  } else {
    return (
      <figure style={common.zeroed}>
        <img src={src} alt={alt} style={actualStyle}/>
      </figure>
    );
  }
}

export default Figure;