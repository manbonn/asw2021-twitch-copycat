import React from 'react';
import logo from "assets/images/logo.png";
import 'styles/spin-animation.css';
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import LocalizedStrings from "react-localization";

function SpinningCharmander() {
  useLanguage(strings)

  return (
    <div style={styles.content}>
      <img src={logo} className="logo spinning" alt="A spinning Charmander"/>
      <P>{strings.spinning}</P>
    </div>
  );
}

const styles = {
  content: {
    textAlign: "center",
    minHeight: "100vh",

    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    fontSize: "calc(10px + 2vmin)",
  },
  logo: {
    height: "40vmin",
    pointerEvents: "none",
  }
}
const strings = new LocalizedStrings({
  en: {
    spinning: "Stay tuned for more Charmander!"
  },
  it: {
    spinning: "Torna a trovarci per altro Charmander!"
  },
  ru: {
    spinning: "Вернитесь, чтобы увидеть больше Чармандера!"
  }
});

export default SpinningCharmander;