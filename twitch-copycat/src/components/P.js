import React from 'react';
import {useTheme} from "contexts/ThemeContext";
import {appendStyle, common} from "styles/common";

function P({style, children}) {
  const theme = useTheme()
  const actualStyle = {...common.zeroed, ...style}

  return (
    <p style={appendStyle(actualStyle, "color", theme.primaryText)}>
      {children}
    </p>
  );
}

export default P;