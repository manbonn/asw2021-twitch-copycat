import React from 'react';

function Row({children}) {
  return (
    <div style={styles.row}>
      {children}
    </div>
  );
}

const styles = {
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  }
}

export default Row;