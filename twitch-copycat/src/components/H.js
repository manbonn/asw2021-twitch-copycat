import React from 'react';
import {useTheme} from "contexts/ThemeContext";
import {appendStyle, common} from "styles/common";

function H({number, style, children}) {
  const theme = useTheme()
  const actualStyle = {...common.empty, ...style}

  switch (number) {
    case 1:
      return <h1 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h1>
    case 2:
      return <h2 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h2>
    case 3:
      return <h3 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h3>
    case 4:
      return <h4 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h4>
    case 5:
      return <h5 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h5>
    case 6:
      return <h6 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h6>
    default:
      return <h7 style={appendStyle(actualStyle, "color", theme.primaryText)}>{children}</h7>
  }
}

export default H;