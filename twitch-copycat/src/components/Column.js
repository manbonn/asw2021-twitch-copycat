import React from 'react';

function Column({children}) {
  return (
    <div style={styles.column}>
      {children}
    </div>
  );
}

const styles = {
  column: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center"
  }
}

export default Column;