import A from "components/A";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React from "react";
import LocalizedStrings from "react-localization";
import {appendStyle, common} from "styles/common";

function Info({followers, title, viewers, tags}) {
  useLanguage(strings)
  const theme = useTheme()

  const bioStyle = appendStyle(
    styles.bordered,
    "borderColor", theme.dilutedSecondaryColor)

  let categoryStyle = {...styles.categories, ...common.plainLink}
  categoryStyle = appendStyle(categoryStyle, "backgroundColor", theme.dilutedSecondaryColor)
  categoryStyle = appendStyle(categoryStyle, "color", theme.dilutedSecondaryText)

  return (
    <div style={{...common.row, ...styles.spaced, ...bioStyle}}>
      <div style={common.column}>
        <P>{title}</P>
        <div style={{...common.row, ...styles.wrapping}}>
          {tags.map(it => {
            return (<A style={categoryStyle}>{it.toString()}</A>)
          })}
        </div>
      </div>
      <div style={common.column}>
        <P>{followers + " " + (followers === "1" ? strings.follower : strings.followers)}</P>
        <P>{viewers + " " + (followers === "1" ? strings.viewer : strings.viewers)}</P>
      </div>
    </div>
  )
}

const strings = new LocalizedStrings({
  en: {
    follower: "follower",
    viewer: "viewer",
    followers: "followers",
    viewers: "viewers"
  },
  it: {
    follower: "follower",
    viewer: "spettatore",
    followers: "follower",
    viewers: "spettatori"
  },
  ru: {
    follower: "последователь",
    viewer: "зритель",
    followers: "последователи",
    viewers: "зрители"
  }
});

const styles = {
  spaced: {
    justifyContent: "space-between"
  },
  bordered: {
    borderStyle: "solid",
    borderWidth: "7px",
    borderRadius: "5%",
    margin: "2px"
  },
  player: {
    width: "66vw",
    height: "auto",
  },
  chat: {
    width: "26vw",
    height: "auto"
  },
  wrapping: {
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  categories: {
    borderStyle: "solid",
    borderRadius: "15px",
    borderWidth: "1px",
  },
}

export default Info;