import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React from "react";
import LocalizedStrings from "react-localization";
import {appendStyle, common} from "styles/common";

function OfflineInfo({followers}) {
  useLanguage(strings)
  const theme = useTheme()

  const bioStyle = appendStyle(
    styles.bordered,
    "borderColor", theme.dilutedSecondaryColor)

  return (
    <div style={{...common.row, ...styles.spaced, ...bioStyle}}>
      <div style={common.column}>
        <P>{followers + " " + (followers === "1" ? strings.follower : strings.followers)}</P>
      </div>
    </div>
  )
}

const strings = new LocalizedStrings({
  en: {
    follower: "follower",
    followers: "followers",
  },
  it: {
    follower: "follower",
    followers: "follower",
  },
  ru: {
    follower: "последователь",
    followers: "последователи",
  }
});

const styles = {
  spaced: {
    justifyContent: "space-between"
  },
  bordered: {
    borderStyle: "solid",
    borderWidth: "7px",
    borderRadius: "5%",
    margin: "2px"
  },
  player: {
    width: "66vw",
    height: "auto",
  },
  chat: {
    width: "26vw",
    height: "auto"
  },
  wrapping: {
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  categories: {
    borderStyle: "solid",
    borderRadius: "15px",
    borderWidth: "1px",
  },
}

export default OfflineInfo;
