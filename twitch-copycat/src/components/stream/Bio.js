import H from "components/H";
import {useLanguage} from "contexts/LanguageContext";
import React from 'react';
import LocalizedStrings from "react-localization";
import parse from 'html-react-parser';

function Bio({username}) {
  const temporaryWIP = "<div><p>Managing your bio is disabled at the moment. Work in progress</p><img src=\"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/63cbfa45-eaf7-4e02-852f-0548601f6954/d41pxkx-100c295d-0d07-414f-9cfd-70970c414593.jpg/v1/fill/w_900,h_675,q_75,strp/charmander_w_i_p_with_colour_by_rainbowmorphine_d41pxkx-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9Njc1IiwicGF0aCI6IlwvZlwvNjNjYmZhNDUtZWFmNy00ZTAyLTg1MmYtMDU0ODYwMWY2OTU0XC9kNDFweGt4LTEwMGMyOTVkLTBkMDctNDE0Zi05Y2ZkLTcwOTcwYzQxNDU5My5qcGciLCJ3aWR0aCI6Ijw9OTAwIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.z0xnjrD0rW-41aCjkTl4gxxUEJHna8zPO4Y_xNRkn9c\" alt='WorkInProgress.jpg'/></div>"
  const strings = new LocalizedStrings({
    en: {
      username: username,
      bio: username + "'s bio"
    },
    it: {
      username: username,
      bio: "La bio di " + username
    },
    ru: {
      username: username,
      bio: "Биография"
    }
  });
  useLanguage(strings)


  return (
    <div>
      <H number={3}>{strings.bio}</H>
      {parse(temporaryWIP)}
    </div>
  );
}


export default Bio;