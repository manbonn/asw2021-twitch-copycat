import {Tab, Tabs, TextareaAutosize, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {TabContext, TabPanel} from "@material-ui/lab";
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React, {useContext, useState} from 'react';
import LocalizedStrings from "react-localization";
import {appendStyle, common} from "styles/common";
import ChatIcon from '@material-ui/icons/Chat';
import PeopleIcon from '@material-ui/icons/People';
import SettingsIcon from '@material-ui/icons/Settings';
import io from "socket.io-client"
import {UserContext} from "../../contexts/UserContext";

function Chat({style}) {
  useLanguage(strings)
  const theme = useTheme()
  const [tabIndex, setTabIndex] = useState(0)
  const [hidden, setHidden] = useState(false)
  const [sentMessage, setSentMessage] = useState("")
  const [messages, setMessages] = useState([])
  const socket = io("localhost:3500")


  const handleChange = (event, newValue) => {
    if (newValue === 0) {
      setHidden(false)
    } else {
      setHidden(true)
    }
    setTabIndex(newValue);
  };

  function sendMessage(message) {
    console.log(message)
    socket.emit("chat message", message)
    setSentMessage("")
  }

  socket.on('chat message', function(message){
    setMessages(messages => [...messages, message])
  });

  return (
    <TabContext value={tabIndex.toString()}>
      <div style={{...common.column, ...style}}>
        <Tabs value={tabIndex} onChange={handleChange} scrollButtons={"off"} variant={"fullWidth"} centered>
          <Tab style={appendStyle(styles.tab, "color", theme.primaryText)} icon={<ChatIcon/>}
               aria-label={strings.chat}/>
          <Tab style={appendStyle(styles.tab, "color", theme.primaryText)} icon={<PeopleIcon/>}
               aria-label={strings.spectators}/>
          <Tab style={appendStyle(styles.tab, "color", theme.primaryText)} icon={<SettingsIcon/>}
               aria-label={strings.settings}/>
        </Tabs>
        <TabPanel style={styles.tabPanel} value={"0"} index={0}>
          <ChatPanel messages={messages}/>
        </TabPanel>
        <TabPanel style={styles.tabPanel} value={"1"} index={1}>
          <UsersPanel/>
        </TabPanel>
        <TabPanel style={styles.tabPanel} value={"2"} index={2}>
          <SettingsPanel/>
        </TabPanel>
        <Input message={sentMessage} sender={sendMessage} setter={setSentMessage} hidden={hidden} style={{justifySelf: "center"}}/>
      </div>
    </TabContext>
  );
}

function Input({message, setter, sender, hidden}) {
  const loggedUser = useContext(UserContext)
  let actualUser
  if(loggedUser === undefined) {
    actualUser = "undefined"
  } else {
    actualUser = loggedUser.info[0].username
  }

  return (
    hidden
      ?
      <></>
      :
      <div style={common.row}>
        <TextField
          onChange={(event) => {
            setter(event.target.value)
          }}
          value={message}
          style={styles.fill}/>
        <Button onClick={() => sender({sender:actualUser, message:message})}>{strings.send}</Button>
      </div>
  );
}

function ChatPanel({messages}) {
  messages.forEach(it => console.log(it))

  return (
    <div>
      <div style={styles.fill}>
        <Textarea></Textarea>
      </div>
    </div>
  );
}

function UsersPanel() {
  return (
    <div>
      <P>User panel</P>
    </div>
  );
}

function SettingsPanel() {
  return (
    <div>
      <P>Settings panel</P>
    </div>
  );
}

const styles = {
  tab: {
    padding: "0",
    margin: "0",
    width: "8vw",
    maxWidth: "8vw",
    minWidth: "8vw"
  },
  tabPanel: {
    flex: "1 0 auto",
    padding: "15px",
    border: "2px red solid",
    backgroundColor: "green"
  },
  fill: {
    flex: "1 0 auto"
  }
}

const strings = new LocalizedStrings({
  en: {
    send: "Send",
    chat: "Chat",
    spectators: "Spectators",
    settings: "Settings",
  },
  it: {
    send: "Invia",
    chat: "Chat",
    spectators: "Spettatori",
    settings: "Impostazioni",
  },
  ru: {
    send: "Отправить",
    chat: "Чат",
    spectators: "Зрители",
    settings: "Настройки",
  }
});

export default Chat;