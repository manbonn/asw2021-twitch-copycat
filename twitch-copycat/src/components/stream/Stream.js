import Bio from "components/stream/Bio";
import Chat from "components/stream/Chat";
import Player from "components/stream/Player";
import Info from "components/stream/Info";

import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React from 'react';
import LocalizedStrings from "react-localization";
import {appendStyle, common} from "styles/common";

function Stream({username, liveSource, bio, followers, title, category, viewers, tags}) {
  useLanguage(strings)
  const theme = useTheme()

  const playerStyle = appendStyle({
    ...styles.bordered,
    ...styles.player
  }, "borderColor", theme.dilutedSecondaryColor)

  const chatStyle = appendStyle({
    ...styles.bordered,
    ...styles.chat
  }, "borderColor", theme.dilutedSecondaryColor)

  const bioStyle = appendStyle(
    styles.bordered,
    "borderColor", theme.dilutedSecondaryColor)

  return (
    <div style={common.column}>
      <div style={common.row}>
        <div style={common.column}>
          <Player style={playerStyle} liveSource={liveSource}/>
          <Info style={bioStyle} followers={followers} title={title} category={category} viewers={viewers} tags={tags}/>
        </div>
        <Chat style={chatStyle}/>
      </div>
      <div style={bioStyle}>
        <Bio username={username} html={bio}/>
      </div>
    </div>
  );
}

const strings = new LocalizedStrings({
  en: {
    follower: "follower",
    viewer: "viewer",
    followers: "followers",
    viewers: "viewers"
  },
  it: {
    follower: "follower",
    viewer: "spettatore",
    followers: "follower",
    viewers: "spettatori"
  },
  ru: {
    follower: "последователь",
    viewer: "зритель",
    followers: "последователи",
    viewers: "зрители"
  }
});

const styles = {
  spaced: {
    justifyContent: "space-between"
  },
  bordered: {
    borderStyle: "solid",
    borderWidth: "7px",
    borderRadius: "5%",
    margin: "2px"
  },
  player: {
    width: "66vw",
    height: "auto",
  },
  chat: {
    width: "26vw",
    height: "auto"
  },
  wrapping: {
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  categories: {
    borderStyle: "solid",
    borderRadius: "15px",
    borderWidth: "1px",
  },
}

export default Stream;