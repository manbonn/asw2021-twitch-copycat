import {useLanguage} from "contexts/LanguageContext";
import React from 'react';
import LocalizedStrings from "react-localization";
import ReactHlsPlayer from 'react-hls-player';

function Player({}) {
  useLanguage(strings)

  return (
    <ReactHlsPlayer
      src="https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
      autoPlay={false}
      controls={true}
      width="100%"
      height="auto"
    />
  );
}

const strings = new LocalizedStrings({
  en: {
    unsupported: "Your browser does not support this video format"
  },
  it: {
    unsupported: "Il tuo browser non supporta questo formato video"
  },
  ru: {
    unsupported: "Ваш браузер не поддерживает этот формат видео"
  }
});


export default Player;