import React from 'react';
import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import LocalizedStrings from "react-localization";
import {appendStyle} from "styles/common";

function Disclaimer() {
  useLanguage(strings)
  const theme = useTheme()

  return (
    <>
      <P style={appendStyle(styles.disclaimer, "color", theme.dilutedSecondaryText)}>{strings.first}</P>
      <P style={appendStyle(styles.disclaimer, "color", theme.dilutedSecondaryText)}>{strings.second}</P>
      <P style={appendStyle(styles.disclaimer, "color", theme.dilutedSecondaryText)}>{strings.third}</P>
    </>
  );
}

const styles = {
  disclaimer: {
    textAlign: "center",
    fontSize: 9
  }
}

const strings = new LocalizedStrings({
  en: {
    first: "This website has been built as a final project for the course Web Application and Services, attended at the University of Bologna.",
    second: "Any copyright infringement is asked to be pardoned, as in a couple of months I will tear down the website anyway.",
    third: "For content removal, do not hesitate to contact me through one of the means below.",
  },
  it: {
    first: "Questo sito web è stato sviluppato come progetto finale per il corso di Applicazioni e Servizi Web, frequentato presso l'Università di Bologna",
    second: "Si prega di perdonare qualsiasi violazione di copyright, il sito verrà comunque rimosso in un paio di mesi",
    third: "Per la rimozione di qualsiasi tipo di contenuto, si prega di contattarmi tramite i mezzi messi a disposizione",
  },
  ru: {
    first: "Этот сайт последний проект для \"Applicazioni e Servizi Web\", учиться на Болонском университете.",
    second: "",
    third: "",
  }
});

export default Disclaimer;