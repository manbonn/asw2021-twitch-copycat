import P from "components/P";
import {useLanguage} from "contexts/LanguageContext";
import {useTheme} from "contexts/ThemeContext";
import React from 'react';
import LocalizedStrings from "react-localization";
import {appendStyle} from "styles/common";

function Copyright() {
  useLanguage(strings)
  const theme = useTheme()

  return (
    <P style={appendStyle(styles.copyright, "color", theme.dilutedSecondaryText)}>{strings.copyright}</P>
  );
}

const styles = {
  copyright: {
    textAlign: "center",
    fontSize: 9
  }
}

const strings = new LocalizedStrings({
  en: {
    copyright: "© Copyright mb@charmander.me 2021 - 2021. This might not be true."
  },
  it: {
    copyright: "© Copyright mb@charmander.me 2021 - 2021. Non credo lo sia davvero."
  },
  ru: {
    copyright: "© Copyright mb@charmander.me 2021 - 2021. Это может быть неправдой"
  }
});

export default Copyright;