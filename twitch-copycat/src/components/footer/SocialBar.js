import facebookOriginal from "assets/images/facebook-original.png";
import instagramOriginal from "assets/images/instagram-original.png";
import twitterOriginal from "assets/images/twitter-original.png";
import vkOriginal from "assets/images/vk-original.png";
import facebookDark from "assets/images/facebook-dark.png";
import instagramDark from "assets/images/instagram-dark.png";
import twitterDark from "assets/images/twitter-dark.png";
import vkDark from "assets/images/vk-dark.png";

import Figure from "components/Figure";
import {ThemeContext} from "contexts/ThemeContext";
import React, {useContext} from 'react';
import {common} from "styles/common";

function SocialBar() {
  const theme = useContext(ThemeContext)

  if (theme === "dark") {
    return (
      <div style={styles.social}>
        <a href={"https://instagram.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={instagramDark} style={styles.socialIcon} alt={"Instagram logo"}/>
        </a>
        <a href={"https://facebook.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={facebookDark} style={styles.socialIcon} alt={"Facebook logo"}/>
        </a>
        <a href={"https://twitter.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={twitterDark} style={styles.socialIcon} alt={"Twitter logo"}/>
        </a>
        <a href={"https://vk.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={vkDark} style={styles.socialIcon} alt={"VK logo"}/>
        </a>
      </div>
    );
  } else {
    return (
      <div style={styles.social}>
        <a href={"https://instagram.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={instagramOriginal} style={styles.socialIcon} alt={"Instagram logo"}/>
        </a>
        <a href={"https://facebook.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={facebookOriginal} style={styles.socialIcon} alt={"Facebook logo"}/>
        </a>
        <a href={"https://twitter.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={twitterOriginal} style={styles.socialIcon} alt={"Twitter logo"}/>
        </a>
        <a href={"https://vk.com"} style={common.zeroed} target="_blank" rel="noreferrer noopener">
          <Figure src={vkOriginal} style={styles.socialIcon} alt={"VK logo"}/>
        </a>
      </div>
    );
  }
}

const styles = {
  social: {
    display: "flex",
    flexDirection: "row",
    alignItems: "",
    justifyContent: ""
  },
  socialIcon: {
    marginLeft: "1vw",
    marginRight: "1vw",
    width: "32px",
    height: "32px"
  }
}

export default SocialBar;
