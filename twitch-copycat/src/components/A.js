import {useTheme} from "contexts/ThemeContext";
import React from 'react';
import {appendStyle, common} from "styles/common";

function A({children, style, ...props}) {
  const theme = useTheme()
  const actualStyle = {...common.empty, ...style}

  return (
    <a style={appendStyle(actualStyle, "color", theme.linkColorMain)} {...props}>
      {children}
    </a>
  );
}

export default A;