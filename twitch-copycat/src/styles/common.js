const common = {
  empty: {},
  zeroed: {
    margin: "0px",
    padding: "0px"
  },
  row: {
    display: "flex",
    flexDirection: "row"
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  rowBetween: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  rowEvenly: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly"
  },
  columnBetween: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  columnEvenly: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly"
  },
  plainLink: {
    textDecoration: "none"
  }
}

function appendStyle(base, property, toBeAppended) {
  return {
    ...base,
    [property]: toBeAppended
  }
}

export {
  common,
  appendStyle
}