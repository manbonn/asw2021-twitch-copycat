import 'styles/spin-animation.css';
import {LiveRoutesManipulationContext, ReplayRoutesManipulationContext} from "contexts/InnerRoutingContext";
import {LanguageContext, LanguageManipulationContext} from "contexts/LanguageContext";
import {ThemeContext, ThemeManipulationContext} from "contexts/ThemeContext";
import {UserContext, UserManipulationContext} from "contexts/UserContext";
import MainPage from "MainPage";
import React, {useEffect, useState} from "react";

function App() {
  const [language, setLanguage] = useState("en")
  const [theme, setTheme] = useState("original")
  const [liveRoutes, setLiveRoutes] = useState([])
  const [replayRoutes, setReplayRoutes] = useState([])
  const [user, setUser] = useState(undefined)

  useEffect(() => {

  }, [user])

  return (
    <ThemeContext.Provider value={theme}>
      <LanguageContext.Provider value={language}>
        <LanguageManipulationContext.Provider value={(lang) => setLanguage(lang)}>
          <ThemeManipulationContext.Provider value={(theme) => setTheme(theme)}>
            <LiveRoutesManipulationContext.Provider value={(live) => setLiveRoutes(live)}>
              <ReplayRoutesManipulationContext.Provider value={(replay) => setReplayRoutes(replay)}>
                <UserContext.Provider value={user}>
                  <UserManipulationContext.Provider value={(user) => setUser(user)}>
                    <MainPage liveRoutes={liveRoutes} replayRoutes={replayRoutes}/>
                  </UserManipulationContext.Provider>
                </UserContext.Provider>
              </ReplayRoutesManipulationContext.Provider>
            </LiveRoutesManipulationContext.Provider>
          </ThemeManipulationContext.Provider>
        </LanguageManipulationContext.Provider>
      </LanguageContext.Provider>
    </ThemeContext.Provider>
  );
}

export default App;
